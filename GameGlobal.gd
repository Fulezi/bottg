extends Node


var player_position : Vector3


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func object_get_hit(body : Node, power : float):
	if body.is_in_group("Hittable"):
		#body.hit(power)
		print("Pouet: ", body, ". Power: ", power)

func get_player_position() -> Vector3:
	return player_position

func set_player_position(var pos : Vector3):
	player_position = pos
