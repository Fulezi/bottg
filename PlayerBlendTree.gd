extends KinematicBody

var velocity = Vector3.ZERO
var impulse = Vector3.ZERO

var animation_state : AnimationNodeStateMachinePlayback

var lives = 3

var floor_normal = Vector3.ZERO

# Speed at which the Walk animation with a scale on 1 will fit.
# i.e. If the character move 1.5 unit per second forward, and the animation run
# with a time scale of one, the foot will not slip.
var animation_sync_point = 1.5
# Input (from the joy-pad) to move the character
var locomotion_input : Vector3 = Vector3.ZERO
# I.e. the absolute squared length of the locomotion input vector
var locomotion_abs : float = 0.0 # TODO actually not used
# Max reachable speed
var max_speed = 3

var running : bool = false

var action_block = false
var action_attack = false
var action_tornado = false
var action_bombing = false
var action_bombing_cooldown = 0
var on_floor = false

func is_on_floor() -> bool:
	return on_floor

func _ready():
	Helper.print_property_list($AnimationTree)

var time = 0
var direction = 1
var idle_walk_blend = 0

func set_locomotion(locomotion : Vector3):
	locomotion_input = locomotion
	locomotion_abs = abs(locomotion_input.x) + abs(locomotion_input.z)

func attack():
	action_attack = true
	
func tornado():
	action_tornado = true
	
func start_block():
	if action_block:
		return
	$AnimationTree.set("parameters/SeekBlock/seek_position", 0)
	action_block = true

func stop_block():
	action_block = false

func _process(delta):
	var horizontal_speed = abs(Vector2(velocity.x, velocity.z).length())
	var animation_scale = horizontal_speed / animation_sync_point

	#TODO handle correct (smooth) transition between the animations
	if horizontal_speed <= 0.01:
		$AnimationTree.get("parameters/StaticStateMachine/playback").travel("Idle")
	else:
		if running:
			$AnimationTree.get("parameters/StaticStateMachine/playback").travel("marche")
			$AnimationTree.set("parameters/StaticStateMachine/marche/BlendRunWalk/blend_amount", 1.0)
			$AnimationTree.set("parameters/StaticStateMachine/marche/TimeScale/scale", 1.0)
		else:
			$AnimationTree.set("parameters/StaticStateMachine/marche/BlendRunWalk/blend_amount", 0)
			$AnimationTree.get("parameters/StaticStateMachine/playback").travel("marche")
			$AnimationTree.set("parameters/StaticStateMachine/marche/TimeScale/scale", animation_scale)
	
	#if not is_on_floor() and velocity.y < 0:
	action_bombing_cooldown -= delta
	if action_bombing or action_bombing_cooldown > 0:
		$AnimationTree.get("parameters/StaticStateMachine/playback").travel("dive-loop")
		$AnimationTree.set("parameters/StaticStateMachine/marche/BlendRunWalk/blend_amount", 1.0)
		$AnimationTree.set("parameters/StaticStateMachine/marche/TimeScale/scale", 1.0)
	
	if action_tornado:
		action_tornado = false
		$SwingSFX.play()
		$AnimationTree.set("parameters/OneShotTransition/current", 1)
		$AnimationTree.set("parameters/OneShot/active", true)
	if action_attack:
		action_attack = false
		$AnimationTree.set("parameters/OneShotTransition/current", 0)
		$AnimationTree.set("parameters/OneShot/active", true)
		# TODO: No need for on site attack?
#		if horizontal_speed > 0:
#			# While walking attack
#			$AnimationTree.set("parameters/OneShotTransition/current", 0)
#			$AnimationTree.set("parameters/OneShot/active", true)
#		else:
#			# On site attack
#			$AnimationTree.get("parameters/StaticStateMachine/playback").travel("attack")
	if action_block:
		$AnimationTree.set("parameters/BlendBlock/blend_amount", 1)
	else:
		$AnimationTree.set("parameters/BlendBlock/blend_amount", 0)

func _physics_process(delta):
	move_player(delta)
	impulse = impulse * 0.90

func move_player(delta):
	var locomotion_clamped_strength = clamp(locomotion_abs, 0, 1)
	var speed = locomotion_clamped_strength * max_speed
	velocity = impulse + -global_transform.basis.z * speed
	
	if action_bombing_cooldown > 0:
		velocity = Vector3.ZERO
		
	velocity.y -= 9.2 * 2
	
	var max_slides = 4
	var max_angle = deg2rad(40)
	var up_dir = Vector3.UP
	# TODO Snap must be disabled for jumping
	#velocity = move_and_slide_with_snap(velocity, Vector3.DOWN * 0.1, 
	#	up_dir, true, max_slides, max_angle)
	velocity = move_and_slide(velocity, Vector3.UP, 
		true, max_slides, max_angle)
	
	if $RayCast.is_colliding() and velocity.y <= 0.0:
		var y = $RayCast.get_collision_point().y 
#		var div = 1
#		if $RayCast2.is_colliding():
#			y += $RayCast2.get_collision_point().y
#			div += 1
#		if $RayCast3.is_colliding():
#			y += $RayCast3.get_collision_point().y
#			div += 1
#		y /= div
		global_transform.origin.y = y
		on_floor = true
	else:
		on_floor = false
	
	floor_normal = Vector3.ZERO
	for i in get_slide_count():
		var c = get_slide_collision(i)
		floor_normal += c.normal
	floor_normal = (floor_normal / get_slide_count()).normalized()

func hit(power : float, hit_point : Vector3):
	assert(false)


func _on_AttackArea_body_entered(body : Node):
	var strength = 3
	
	#var a = $AnimationTree.parameters.OneShot.remaining
	var is_tornading = $AnimationTree.get("parameters/OneShot/remaining") > 0 and $AnimationTree.get("parameters/OneShotTransition/current") == 1
	if is_tornading:
		strength = 30
	if body != self and body.is_in_group("Hittable"):
		body.hit(strength, $rig/Skeleton/BoneAttachment/Sword/AttackArea/AttackHithape.global_transform.origin)
