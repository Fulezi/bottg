shader_type spatial;
render_mode unshaded;

uniform sampler2D albedotex : hint_albedo;
uniform sampler2D noisetex : hint_white;

varying flat float random;
varying flat float delta;

void vertex()
{
  random = float(INSTANCE_CUSTOM.r);
  delta = float(INSTANCE_CUSTOM.g);

  // Billboard mode
  vec4 cameraPos = CAMERA_MATRIX[3];
  vec3 cameraUp = CAMERA_MATRIX[2].xyz;
  vec4 particlePos = WORLD_MATRIX[3];

  vec3 dir = normalize(cameraPos.xyz - particlePos.xyz);
  vec3 right = cross(cameraUp, dir);
  WORLD_MATRIX[0] = vec4(right, 0.0);
  WORLD_MATRIX[1] = vec4(cameraUp, 0.0);
  WORLD_MATRIX[2] = vec4(dir, 0.0);
  MODELVIEW_MATRIX = INV_CAMERA_MATRIX * WORLD_MATRIX;
}

void fragment()
{
  //vec2 uv = vec2(UV.x, UV.y + TIME + random);
  vec2 uv = vec2(UV.x, UV.y + random * 3.0 + TIME / 2.0);
  float alpha = max(texture(noisetex, uv).r, 0.0);
  vec4 albedo = texture(albedotex, UV);
  ALBEDO = vec3(1.0);
  ALPHA = max(min(albedo.a, alpha) - delta, 0.0);
}
