shader_type particles;

uniform float radiusInput;

float random(vec2 st)
{
  return fract(sin(dot(st.xy,
		       vec2(12.9898,78.233))) *
	       43758.5453123);
}

float randomRange(float seed, float minValue, float maxValue)
{
  float r = random(vec2(seed, seed));
  return mix(minValue, maxValue, r);
}


void vertex()
{
  float tau = ((2.0 * 3.14) / 10.0) * float(INDEX);
  //	VELOCITY.x = cos(tau);
  //	VELOCITY.z = sin(tau);
  //	VELOCITY = normalize(VELOCITY);
  //	VELOCITY *= 12.0;

  if (RESTART) {
    // Elapsed time
    CUSTOM.g = 0.0;
    // Random value
    CUSTOM.r = random(vec2(float(INDEX), float(INDEX)));
    // Radius
    CUSTOM.b = randomRange(CUSTOM.r, 0.9 * radiusInput, 1.2 * radiusInput);
    // Movement randomness
    CUSTOM.a = randomRange(CUSTOM.r, 0.8, 1.2);
  }
  float radius = CUSTOM.b;
  float movementRandomness = CUSTOM.a;
  vec2 pos = normalize(vec2(cos(tau), sin(tau))) * radius;
  TRANSFORM[3].xz = pos;
  TRANSFORM[3].y += DELTA * movementRandomness;

  tau += TIME * movementRandomness;
  TRANSFORM[0] = vec4(cos(tau), sin(tau), 0.0, 0.0);
  TRANSFORM[1] = vec4(-sin(tau), cos(tau), 0.0, 0.0);
  TRANSFORM[2] = vec4(0.0, 0.0, 1.0, 0.0);
  //COLOR.a = max(COLOR.a - DELTA, 0.0);
  CUSTOM.g += DELTA;
}
