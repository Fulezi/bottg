extends Area


var creator : Node = null


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


var count = 2
func _process(delta):
	if count == 2:
		$Smoke3D.emitting = true
		$ExplosionSFX.play()
	for body in get_overlapping_bodies():
		if body.is_in_group("Hittable") and body != creator:
			body.hit(30, global_transform.origin)
	count -= 1
	if count <= 0:
		$CollisionShape.disabled = true
	if not $Smoke3D.emitting:
		queue_free()
