extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func print_property_list(parent : Object, parent_name : String = ""):
	if parent_name.empty():
		print(parent.get_name())
	else:
		print(parent_name)
	var list = parent.get_property_list()
	for prop in list:
		print("	> " + prop["name"])
	print("---")
