extends Spatial

onready var hotspot = get_node("SkeletonWarriorManu3/rig/Skeleton/BoneAttachment")
onready var ParticleSpawner = get_node("SkeletonWarriorManu3/Particles")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var pos = hotspot.global_transform.origin
	var center = get_node("SkeletonWarriorManu3").global_transform.origin
	var dir = (pos - center).normalized()
	var offset = dir * 1.2
	offset.y = 0
	pos += offset
	ParticleSpawner.process_material.set_shader_param("globalPos", pos)
	ParticleSpawner.process_material.set_shader_param("velocityDir", offset)
