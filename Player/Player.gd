extends "res://PlayerBlendTree.gd"

var jumping  = 0

onready var EjectionAreaGPE = preload("res://GPE/EjectionArea/EjectionArea.tscn")

func _ready():
	pass
	
func update_inputs():
	locomotion_input = Vector3.ZERO
	locomotion_input.z += Input.get_action_strength("player_move_forward")
	locomotion_input.z -= Input.get_action_strength("player_move_backward")
	locomotion_input.x -= Input.get_action_strength("player_sidestep_left") 
	locomotion_input.x += Input.get_action_strength("player_sidestep_right")
	set_locomotion(locomotion_input)
	var locomotion_length = abs(locomotion_input.z) + abs(locomotion_input.x)
	if locomotion_length >= 0.8:
		running = true
		self.max_speed = 8
	if locomotion_length < 0.5:
		running = false
		self.max_speed = 3


func update_look_at(delta):
	var camera_sight = get_parent().get_node("OuterGimbal").transform.basis
	var dir = (-camera_sight.z * locomotion_input.z + camera_sight.x * locomotion_input.x)
	if dir.length_squared() > 0:
		var xform = global_transform.looking_at(global_transform.origin + dir, Vector3.UP)
		#TODO Need to continue the smooth until done. Otherwise it won't point
		# the correct direction
		#global_transform = global_transform.interpolate_with(xform, delta * 10.0).orthonormalized()
		global_transform = xform

func _process(delta):
	update_look_at(delta)
	update_inputs()
	if is_on_floor() or is_on_wall():
		jumping = 2
	elif jumping == 2:
		jumping = 1
	
	if is_on_floor():
		if action_bombing:
			var gpe = EjectionAreaGPE.instance()
			gpe.creator = self
			get_tree().root.add_child(gpe)
			gpe.global_transform.origin = global_transform.origin
			action_bombing_cooldown = 0.8
		action_bombing = false
	
	if Input.is_action_just_pressed("player_jump"):
		jump()
	if Input.is_action_just_pressed("player_action"):
		self.impulse = self.velocity * 0.5
		$Particles.emitting = true
		tornado()
	if Input.is_action_just_pressed("player_hit"):
		if jumping < 2 and not action_bombing:
			impulse += Vector3.DOWN * 250.0
			action_bombing = true
		else:
			snap_to_closest_target()
			attack()
			$Particles.emitting = true
	if Input.get_action_strength("player_block") > 0:
		start_block()
	else:
# TODO Charge while removing the shield:
#		if self.action_block and self.running:
#			attack()
#			self.impulse = -global_transform.basis.z * 32
		stop_block()
	GameGlobal.set_player_position(global_transform.origin)


func hit(power : float, hit_point : Vector3):
	if self.action_block:
		$BlockSFX.play()
		return
	get_parent().get_node("HumanHitSFX").play()
	print("Player looses a life")

func jump():
	if jumping > 0:
		jumping = jumping - 1
		self.impulse += Vector3(0, 132, 0)

func snap_to_closest_target() -> Vector3:
	var bodies = $SnapArea.get_overlapping_bodies()
	var snap = null
	var min_dist2 = 0
	for body in bodies:
		if body.is_in_group("Hittable") and body != self:
			var dist2 = body.global_transform.origin.length_squared()
			if dist2 < min_dist2 or snap == null:
				snap = body.global_transform.origin
				min_dist2 = dist2
	if snap != null:
		snap.y = global_transform.origin.y # Do not look up/down
		look_at(snap, Vector3.UP)
	return snap
