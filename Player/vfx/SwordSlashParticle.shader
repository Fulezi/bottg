shader_type particles;

uniform vec3 globalPos;
uniform vec3 velocityDir;

float easeOutQuint(float x) {
  return 1.0 - pow(1.0 - x, 5.0);
}

void vertex()
{
  if (RESTART) {
    TRANSFORM[3].xyz = globalPos;
    CUSTOM.x = 0.0;
    VELOCITY = velocityDir;
  }
  CUSTOM.x += DELTA;
  float scale = mix(CUSTOM.x / LIFETIME, 0.6, 1.2);
  TRANSFORM[0].x = scale; // TODO break any rotation
  TRANSFORM[1].y = scale;
  TRANSFORM[2].z = scale;
  TRANSFORM[3].xyz += VELOCITY * DELTA;
  //COLOR = vec4(0.0, 0.0, 0.0, 1.0);

  float v = (CUSTOM.x / LIFETIME);
  COLOR.a = 1.0 - easeOutQuint(v); 
}
