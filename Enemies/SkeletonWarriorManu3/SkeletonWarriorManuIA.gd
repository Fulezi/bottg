extends "res://PlayerBlendTree.gd"

onready var SkeletonBroken = preload("res://Enemies/skeleton-warrior-manu/SkeletonWarriorMeshBroken.tscn")

class StateC:
	func enter():
		pass
	func exit():
		pass
	func execute():
		pass

var last_hit_timer = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

var timer = 0

enum State { MOVE_CLOSER, RETREAT, THREATEN, ATTACK, FIGHT }

var state = State.MOVE_CLOSER
var threaten_timer = 0
var attack_timer = 0

var dir_to_player : Vector3 = Vector3.ZERO

func look_at(pos : Vector3, delta):
	var dir = pos - self.global_transform.origin
	dir.y = 0
	var xform = global_transform.looking_at(global_transform.origin + dir, Vector3.UP)
	global_transform = global_transform.interpolate_with(xform, delta * 10.0).orthonormalized()

func move_to(pos : Vector3, delta):
	look_at(pos, delta)
	var moving = 0.0
	moving = 1.0
	set_locomotion(Vector3(0, 0, moving))
	pass

func retreat():
	state = State.RETREAT
	
func threaten():
	threaten_timer = 0
	start_block()
	state = State.THREATEN

func do_attack():
	attack_timer = 0
	state = State.ATTACK
	self.attack()

func _process(delta):
	var player_position = GameGlobal.get_player_position()
	dir_to_player = player_position - self.global_transform.origin
	dir_to_player.y = 0
	var distance_to_player = dir_to_player.length()
	dir_to_player = dir_to_player.normalized()
	
	# TODO Reflex to attack or block when the player comes
	# TODO Random agressivity level that decides between attack and block on a
	# reflex.
	# TODO Randmize attack timers
	# TODO Threaten attitude (anim)
	set_locomotion(Vector3.ZERO)
	match state:
		State.MOVE_CLOSER:
			if distance_to_player < 12:
				move_to(player_position, delta)
			if distance_to_player <= 3.0:
				threaten()
		State.THREATEN:
			# TODO Keep distance w/ player
			look_at(player_position, delta)
			threaten_timer += delta
			if threaten_timer > 4.0:
				threaten_timer = 0
				stop_block()
				state = State.FIGHT
		State.FIGHT:
			if distance_to_player > 1.6:
				move_to(player_position - (dir_to_player * 1.6), delta)
			else:
				do_attack()
		State.ATTACK:
				# TODO How to know when the attack animation has ended
				look_at(player_position, delta)
				attack_timer += delta
				if attack_timer > 1:
					retreat()
		State.RETREAT:
			move_to(player_position - (dir_to_player * 4), delta)
			if distance_to_player > 3.6:
				threaten()

func _on_Area_body_entered(body):
	# Private/personal area
	if body.is_in_group("Player"):
		pass

func hit(power : float, hit_point : Vector3):
	var dir = (self.global_transform.origin - hit_point).normalized()
	if self.action_block:
		$BlockSFX.play()
		stop_block()
		do_attack()
		if power > 10:
			self.impulse = dir * 100
			self.impulse.y = 30.0
		return
	var broken = SkeletonBroken.instance()
	broken.global_transform = self.global_transform
	broken.set_push_hit(hit_point, dir*3)
	get_tree().get_current_scene().add_child(broken)
	self.queue_free()
