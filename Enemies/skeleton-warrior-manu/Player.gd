extends KinematicBody

var move_speed = 12.0
var velocity = Vector3.ZERO
var impulse = Vector3.ZERO

var animation_state

var lives = 3

var floor_normal = Vector3.ZERO

func hit(power : float, hit_point : Vector3):
	if animation_state.get_current_node() == "ArmatureAction":
		print("Blocked!!!!!!!!!!!")
		return
	print("ARRRRRRRRRRRRRRRRRGHHHH", power)
	var dir = (self.global_transform.origin - hit_point).normalized()
	impulse = dir * 30
	lives -= 1

func _ready():
	animation_state = $AnimationTree.get("parameters/playback")

func _physics_process(delta):
	move_player(delta)
	
func print_property_list(parent):
	print(parent.get_name())
	var list = parent.get_property_list()
	for prop in list:
		print(" > " + prop["name"])
	print("---")
	
func _process(delta):
	if Input.get_action_strength("player_hit"):
		animation_state.travel("Hit")
		velocity.x = 0
		velocity.z = 0
		return
		
	if Input.get_action_strength("player_block"):
		animation_state.travel("ArmatureAction")
		return

func move_player(delta):
	var input = Vector3.ZERO
	
	input.z += Input.get_action_strength("player_move_forward")
	input.z -= Input.get_action_strength("player_move_backward")

# TODO Rename as strafing	
	input.x -= Input.get_action_strength("player_sidestep_left") 
	input.x += Input.get_action_strength("player_sidestep_right")
	
	# get the relative direction
	#var dir = (transform.basis.z * input.z + transform.basis.x * input.x)
	var camera_sight = get_parent().get_node("OuterGimbal").transform.basis
	var dir = (-camera_sight.z * input.z + camera_sight.x * input.x)
	
	if dir.length_squared() > 0:
		var xform = global_transform.looking_at(global_transform.origin + dir, Vector3.UP)
		global_transform = global_transform.interpolate_with(xform, delta * 10.0).orthonormalized()
	
	var move = clamp(abs(input.z) + abs(input.x), 0.0, 1.0) > 0
	if move: 
		animation_state.travel("Walk")
	else:
		animation_state.travel("Idle")
		
	##########################################################
	# USING ROOT MOTION
	##########################################################
	print($AnimationTree.get_root_motion_transform().origin)
	#var acceleration = $AnimationTree.get_root_motion_transform().origin.z / delta
	var acceleration = $AnimationTree.get_root_motion_transform().origin.z / self.get_process_delta_time()
	velocity = -global_transform.basis.z * acceleration
	velocity.y -= 9.2
	
	if impulse.length_squared() > 0.0:
		velocity += impulse
	impulse *= 0.90
	if impulse.length() < 0.1:
		impulse = Vector3.ZERO
	
	var max_slides = 4
	var max_angle = deg2rad(40)
	velocity = move_and_slide_with_snap(velocity, Vector3.DOWN * 0.1, 
		floor_normal, true, max_slides, max_angle)
	
	floor_normal = Vector3.ZERO
	for i in get_slide_count():
		var c = get_slide_collision(i)
		floor_normal += c.normal
	floor_normal = (floor_normal / get_slide_count()).normalized()
