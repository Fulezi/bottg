extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func set_push_hit(hit_point : Vector3, dir : Vector3):
	#dir = dir * 10
	dir *= 0.45
	hit_point.y -= 0.5
	print("Boom")
	$HitSFX.play()
	$RigidBody.apply_impulse(hit_point, dir)
	$RigidBody2.apply_impulse(hit_point, dir)
	$RigidBody3.apply_impulse(hit_point, dir)
	$RigidBody4.apply_impulse(hit_point, dir)
	$RigidBody5.apply_impulse(hit_point, dir)
	$RigidBody6.apply_impulse(hit_point, dir)
