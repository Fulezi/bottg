extends Area


export(NodePath) var parent_path

var parent : Node = null

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("body_entered", self, "object_get_hit")
	if parent_path:
		parent = get_node(parent_path)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func object_get_hit(body : PhysicsBody):
	if body != null and body != parent and body.is_in_group("Hittable"):
		body.hit(5, global_transform.origin)
		print("Poup: ", body.get_name(), "(parent=", parent.get_name(),")")
