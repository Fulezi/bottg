extends KinematicBody

# DEPRECATED Script: To remove


var move_speed = 12.0
var velocity = Vector3.ZERO

var last_hit_timer = 0.0
var animation_state

onready var SkeletonBroken = preload("res://Enemies/skeleton-warrior-manu/SkeletonWarriorMeshBroken.tscn")

func hit(power : float, hit_point : Vector3):
	print("Ouilles", power)
	var broken = SkeletonBroken.instance()
	broken.global_transform = self.global_transform
	var dir = (self.global_transform.origin - hit_point).normalized()
	broken.set_push_hit(hit_point, dir)
	get_parent().add_child(broken)
	self.queue_free()
	
	

# Called when the node enters the scene tree for the first time.
func _ready():
	animation_state = $AnimationTree.get("parameters/playback")


func _physics_process(delta):
	move_player(delta)
	
func _process(delta):	
#	if Input.get_action_strength("player_hit"):
#		animation_state.travel("Hit")
#		velocity.x = 0
#		velocity.z = 0
#		return
	
	last_hit_timer = max(last_hit_timer - delta, 0.0)
	var move = (abs(velocity.x) + abs(velocity.z)) > 0.1
	if move:
		animation_state.travel("Walk")
	else:
		animation_state.travel("Idle")

func move_player(delta):
	var input = Vector3.ZERO
	
#	input.z += Input.get_action_strength("player_move_forward")
#	input.z -= Input.get_action_strength("player_move_backward")
#
## TODO Rename as strafing	
#	input.x -= Input.get_action_strength("player_sidestep_left") 
#	input.x += Input.get_action_strength("player_sidestep_right")
##	var rot = Input.get_action_strength("player_sidestep_left")
##	rot -= Input.get_action_strength("player_sidestep_right")
##	rotate(Vector3.UP, rot * delta)
	
		
	#input = input.normalized()
	
	# get the relative direction
	#var dir = (transform.basis.z * input.z + transform.basis.x * input.x)
#	var camera_sight = get_parent().get_node("Gimbal").transform.basis
#	var dir = (-camera_sight.z * input.z + camera_sight.x * input.x)
#	#look_at(global_transform.origin + dir, Vector3.UP)

	var moving = 0.0
	var player = get_parent().get_node("Player")
	var dir = player.global_transform.origin - self.global_transform.origin
	if dir.length_squared() > 0 and dir.length() < 18.0:
		var xform = global_transform.looking_at(global_transform.origin + dir, Vector3.UP)
		global_transform = global_transform.interpolate_with(xform, delta * 10.0).orthonormalized()
		moving = 0.3

	if dir.length() < 1.8 and last_hit_timer <= 0.0:
		animation_state.travel("Hit")
		last_hit_timer = 2.3

	var acceleration =  clamp(moving, 0.0, 1.0) * move_speed
	velocity = -global_transform.basis.z * acceleration
	velocity.y -= 9.2
	
	velocity = move_and_slide_with_snap(velocity, Vector3.DOWN * 0.1, 
		Vector3.UP, true)
		
