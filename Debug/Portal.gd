extends Spatial

# TODO check why PackedScene crash the game
export(String, FILE, "*.tscn") var scene

var teleporting = false

func _process(delta):
	if teleporting and $TeleportSFX.playing == false:
		get_tree().change_scene(scene)

func _on_Area_body_entered(body):
	if body.is_in_group("Player"):
		$TeleportSFX.play()
		$Particles.process_material.tangential_accel = 16.6
		teleporting = true
