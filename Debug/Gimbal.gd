extends Spatial

var camera_speed = 3.0

var camera_yaw_force = 0
var camera_pitch_force = 0
export var target_path : NodePath
onready var target = get_node(target_path)

# Called when the node enters the scene tree for the first time.
func _ready():
	$InnerGimbal/SpringArm.add_excluded_object(target)


func _process(delta):
	global_transform.origin = target.global_transform.origin
	
	camera_yaw_force -= Input.get_action_strength("player_camera_yaw")
	camera_yaw_force += Input.get_action_strength("player_camera_neg_yaw")
	camera_yaw_force = clamp(camera_yaw_force, -1.0, 1.0)
	
	camera_pitch_force += Input.get_action_strength("player_camer_pitch")
	camera_pitch_force -= Input.get_action_strength("player_camer_inv_pitch")
	camera_pitch_force = clamp(camera_pitch_force, -1.0, 1.0)
	
	var pitch = $InnerGimbal.rotation.x + camera_pitch_force * camera_speed * delta
	pitch = clamp(pitch, -1.2, 0.2)
	$InnerGimbal.rotation.x = pitch

	rotate_object_local(Vector3.UP, camera_yaw_force * delta * camera_speed)
	#$InnerGimbal.rotate_object_local(Vector3.RIGHT, camera_pitch)
	camera_yaw_force *= 0.60
	camera_pitch_force *= 0.60
