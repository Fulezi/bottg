extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var locomotion_input = Vector2.ZERO
	locomotion_input.y += Input.get_action_strength("player_move_forward")
	locomotion_input.y -= Input.get_action_strength("player_move_backward")
	locomotion_input.x -= Input.get_action_strength("player_sidestep_left") 
	locomotion_input.x += Input.get_action_strength("player_sidestep_right")
	
	$CenterContainer/XLabel.set_text("x: %.2f" % locomotion_input.x)
	$CenterContainer/YLabel.set_text("y: %.2f" % locomotion_input.y)
	$CenterContainer/LengthLabel.set_text("length (%.2f): %.2f" % [locomotion_input.x + locomotion_input.y, locomotion_input.length()])
