extends Control

onready var InputGrid = get_node("Options/InputsList/ScrollContainer/Grid")

class KeyUpdate:
	var action_name : String = ""
	var new_key : InputEvent = null
	var old_key : InputEvent = null
	var button : Button = null
	
	func _init():
		pass

var key_update : KeyUpdate = null

func get_text_from_event(event : InputEvent) -> String:
	var text = "(Unknown)"
	if event is InputEventJoypadButton:
		text = Input.get_joy_button_string(event.button_index)
	elif event is InputEventJoypadMotion:
		text = Input.get_joy_axis_string(event.axis)
		text += " (positive)" if event.axis_value > 0.0 else " (negative)"
	elif event is InputEventKey:
		text = OS.get_scancode_string(event.get_scancode_with_modifiers())
	return text

func load_input_map():
	var config = ConfigFile.new()
	var err = config.load("user://Inputs.cfg")
	if err == OK:
		var actions = InputMap.get_actions()
		for action_name in actions:
			if config.has_section_key("inputs", action_name):
				var event = config.get_value("inputs", action_name)
				if event:
					InputMap.action_erase_events(action_name)
					InputMap.action_add_event(action_name, event)
	else:
		print("InputMap Load failed") #TODO
				
func save_input_map():
	var config = ConfigFile.new()
	var actions = InputMap.get_actions()
	for action_name in actions:
		var event = InputMap.get_action_list(action_name)[0]
		config.set_value("inputs", action_name, event)
	var err = config.save("user://Inputs.cfg")
	if err != OK:
		print("InputMap Save failed") # TODO

func _ready():
	$Main.modulate.a = 1.0
	$Options.visible = false
	$Options.modulate.a = 0.0
	
	load_input_map()
	reload_action_buttons()
	
func reload_action_buttons():
	for child in InputGrid.get_children():
		child.queue_free()
	var actions = InputMap.get_actions()
	for action_name in actions:
		if action_name.findn("ui_") == 0:
			continue
		var event = InputMap.get_action_list(action_name)[0]
		var item_label = Label.new()
		item_label.text = action_name
		InputGrid.add_child(item_label)
		var button = Button.new()
		var text = get_text_from_event(event)
		button.text = text
		InputGrid.add_child(button)
		button.connect("pressed", self, "update_key", 
			[event, action_name, button])

func update_key(event : InputEvent, action : String, button : Button):
	key_update = KeyUpdate.new()
	key_update.old_key = event
	key_update.action_name = action
	key_update.button = button
	key_update.button.text = "Press a key..."

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if key_update and key_update.new_key != null:
		var action_name = key_update.action_name
		InputMap.action_erase_events(action_name)
		InputMap.action_add_event(key_update.action_name, key_update.new_key)
		key_update.button.text = get_text_from_event(key_update.new_key)
		key_update = null
		save_input_map()


func _input(event : InputEvent):
	if key_update == null:
		return

	if event is InputEventKey and event.pressed:
		key_update.new_key = event
	elif event is InputEventJoypadButton and event.pressed:
		key_update.new_key = event
	elif event is InputEventJoypadMotion and abs(event.axis_value) > 0.4:
		event.axis_value = 1.0 if event.axis_value > 0.0 else -1.0
		key_update.new_key = event

onready var active_screen : Control = get_node("Main")
var new_active_screen : Control = null

func change_screen(new_screen : Control):
	var easing = Tween.TRANS_QUART
	var type = Tween.EASE_OUT
	var time = 0.8
	
	if new_active_screen != null:
		new_active_screen.visible = false
	$ChangeScreenTween.interpolate_property(active_screen, "modulate:a",
		1.0, 0.0, time, easing, type)
	$ChangeScreenTween.start()
	$ChangeScreenTween.interpolate_property(new_screen, "modulate:a",
		0.0, 1.0, time, easing, type)
	$ChangeScreenTween.start()
	new_screen.visible = true
	new_active_screen = new_screen

func _on_Options_pressed():
	change_screen($Options)


func _on_BackButton_pressed():
	change_screen($Main)


func _on_ChangeScreenTween_tween_all_completed():
	assert(new_active_screen != null)
	active_screen.visible = false
	active_screen = new_active_screen
	new_active_screen = null


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_NewGameButton_pressed():
	get_tree().change_scene("res://Debug/LocomotionGym2WithEnnemi.tscn")


func _on_ResetInputsButton_pressed():
	InputMap.load_from_globals()
	reload_action_buttons()
